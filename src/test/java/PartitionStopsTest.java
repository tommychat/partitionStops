import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PartitionStopsTest {

  @Test
  public void testSliceHasNoStops() {
    assertThat(PartitionStops.run(Collections.EMPTY_LIST))
        .isEqualTo(List.of(Collections.EMPTY_LIST));
  }

  @Test
  public void testSliceContainsOneAddStop() {
    final List<Stop> stops = List.of(new Stop("ADD", 1));

    assertThat(PartitionStops.run(stops))
        .isEqualTo(List.of(stops));
  }

  @Test
  public void testSliceContainsOnlyAddStops() {
    final List<Stop> stops = List.of(
        new Stop("ADD", 1),
        new Stop("ADD", 2),
        new Stop("ADD", 3)

    );
    assertThat(PartitionStops.run(stops))
        .isEqualTo(
            List.of(
                stops
            )
        );
  }

  @Test
  public void testSliceContainsOneMajStop() {
    final List<Stop> stops = List.of(new Stop("MAJ", 1));

    assertThat(PartitionStops.run(stops))
        .isEqualTo(List.of(stops));
  }


  @Test
  public void testSliceContainsOnlyMajStops() {
    final List<Stop> stops = List.of(
        new Stop("MAJ", 1),
        new Stop("MAJ", 2),
        new Stop("MAJ", 3)

    );

    assertThat(PartitionStops.run(stops))
        .isEqualTo(List.of(stops));
  }

  @Test
  public void testSliceBeginWithAnAddStopAndEndWithAMajStop() {
    final List<Stop> stops = List.of(
        new Stop("ADD", 1),
        new Stop("MAJ", 2)
    );

    assertThat(PartitionStops.run(stops))
        .isEqualTo(List.of(stops));
  }

  @Test
  public void testSliceBeginWithAMajStopAndEndWithAnAddStop() {
    final List<Stop> stops = List.of(
        new Stop("MAJ", 1),
        new Stop("ADD", 2)
    );

    assertThat(PartitionStops.run(stops))
        .isEqualTo(List.of(stops));
  }

  @Test
  public void testSliceHasTwoPartitions() {
    final List<Stop> stops = List.of(
        new Stop("MAJ", 1),
        new Stop("ADD", 2),
        new Stop("MAJ", 3),
        new Stop("ADD", 4)
    );

    assertThat(PartitionStops.run(stops))
        .isEqualTo(List.of(
            List.of(
                new Stop("MAJ", 1),
                new Stop("ADD", 2),
                new Stop("MAJ", 3)
            ),
            List.of(
                new Stop("MAJ", 3),
                new Stop("ADD", 4)
            )
        ));
  }
}