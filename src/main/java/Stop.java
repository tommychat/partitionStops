import java.util.Objects;

class Stop {
  private final String type;
  private final int rank;

  public Stop(String type, int rank) {
    this.type = type;
    this.rank = rank;
  }

  public boolean isMaj() {
    return type.equals("MAJ");
  }

  public boolean isADD() {
    return type.equals("ADD");
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Stop stop = (Stop) o;
    return rank == stop.rank &&
        Objects.equals(type, stop.type);
  }

  @Override
  public int hashCode() {

    return Objects.hash(type, rank);
  }

  @Override
  public String toString() {
    return "Stop{" +
        "type='" + type + '\'' +
        ", rank=" + rank +
        '}';
  }
}
